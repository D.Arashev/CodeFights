// Given the string, check if it is a palindrome.

bool checkPalindrome(string inputString)
{
    var mid = inputString.Length/2;

    for (int i = 0; i < mid; i++)
    {
        if (inputString[i] != inputString[inputString.Length-1-i])
        {
            return false;
        }
    }
    return true;
}