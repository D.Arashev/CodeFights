# Given a year, return the century it is in. The first century spans from the year 1 up to and including the year 100, the second - from the year 101 up to and including the year 200, etc.

int centuryFromYear(int year) {
int result = 0 ;
    
if(year > 0){
    result = 1;
}
    
    while(year - 100 > 0){
        result += 1;
        year-=100;
    };
    
    return result;
}
